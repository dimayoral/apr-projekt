package _20210205_DAO_mysql_notReady.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.Monster;

public class MonsterDao implements Dao<Monster> {
    
    public MonsterDao() {

        DbController2.connect();
        DbController2.createMonsterTable();        

        this.save(new Monster("Danger", "100"));
        this.save(new Monster("Poison", "100"));
    }
    
    @Override
    public Optional<Monster> get(int id) {
        // TODO: return Optional.ofNullable(users.get((int) id));
        return Optional.of(DbController2.selectOne(id));
    }
    
    @Override
    public List<Monster> getAll() {
        // TODO: return users;
        return DbController2.selectAll();
    }
    
    @Override
    public void save(Monster monster) {
        DbController2.insertMonster(monster.getRace(), monster.getPower());
    }
    
    @Override
    public void update(Monster monster, String[] params) {

        monster.setRace(Objects.requireNonNull(
            params[0], "Race cannot be null"));
        monster.setPower(params[1]);
        
        // von Tutorial korrigiert, dass User nicht doppelt in Liste
        //users.add(user);
    }
    
    @Override
    public void delete(int id) {
        // TODO: users.remove(user);
        DbController2.delete(id);
    }
}

class DbController2 {
    static Connection conn;

    public static void insertMonster(String race, String power){
        String sql = "INSERT INTO monsters(race,power) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, race);
            pstmt.setString(2, power);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createMonsterTable() {
        String sql = "CREATE TABLE IF NOT EXISTS monsters (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	race text NOT NULL,\n"
                + "	power text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table monsters is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }


    public static Monster selectOne(int id){
        String sql = "SELECT race, power FROM monsters";
        ArrayList<Monster> monsters = new ArrayList<Monster>();

        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Monster monster = new Monster(rs.getString("race"), rs.getString("power"));
                monsters.add(monster);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return monsters.get(id);
    }

    public static ArrayList<Monster>selectAll(){
        String sql = "SELECT race, power FROM monsters";
        ArrayList<Monster> monsters = new ArrayList<Monster>();

        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Monster monster = new Monster(rs.getString("race"), rs.getString("power"));
                monsters.add(monster);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return monsters;
    }

    public static void delete(int id){
        String sql = "DELETE from monsters where id = ?";
        try{
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
