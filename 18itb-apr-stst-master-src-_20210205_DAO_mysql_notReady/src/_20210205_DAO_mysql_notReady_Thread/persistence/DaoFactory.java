package _20210205_DAO_mysql_notReady_Thread.persistence;

public class DaoFactory{

    public static Dao getDao(DaoType type){
       if(type == null){
           return null;
       }
       if (type.equals(DaoType.USER)){
           return new UserDao();
       }
       else if (type.equals(DaoType.MONSTER)){
           return new MonsterDao();
       } 

       return null;
    }

}