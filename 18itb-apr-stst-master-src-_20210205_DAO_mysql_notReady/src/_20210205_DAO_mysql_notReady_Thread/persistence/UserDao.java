package _20210205_DAO_mysql_notReady_Thread.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.User;

public class UserDao implements Dao<User> {
    
    public UserDao() {

        DbController.connect();
        DbController.createUserTable();        

        this.save(new User("John", "john@domain.com"));
        this.save(new User("Susan", "susan@domain.com"));
    }
    
    @Override
    public Optional<User> get(int id) {
        return Optional.of(DbController.selectOne(id));
    }
    
    @Override
    public List<User> getAll() { 
    return DbController.selectAll();
    }
    
    @Override
    public void save(User user) {
        DbController.insertUser(user.getName(), user.getEmail());
    }
    
    @Override
    public void update(User user, String[] params) {

        user.setName(Objects.requireNonNull(
          params[0], "Name cannot be null"));
        user.setEmail(Objects.requireNonNull(
          params[1], "Email cannot be null"));
        
        // von Tutorial korrigiert, dass User nicht doppelt in Liste
        //users.add(user);
    }
    
    @Override
    public void delete(int id) {
        DbController.delete(id);
    }
}

class DbController {
    static Connection conn;

    public static void insertUser(String name, String email){
        String sql = "INSERT INTO users(name,email) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createUserTable() {
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	email text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static User selectOne(int id){
        String sql = "SELECT name, email FROM users";
        ArrayList<User> users = new ArrayList<User>();

        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                User user = new User(rs.getString("name"), rs.getString("email"));
                users.add(user);
            }
        } catch (SQLException e){
            e.printStackTrace();
        } 
        return users.get(id);
    }

    public static ArrayList<User> selectAll(){
        String sql = "SELECT name, email FROM users";
        ArrayList<User> users = new ArrayList<User>();

        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                User user = new User(rs.getString("name"), rs.getString("email"));
                users.add(user);
            }
        } catch (SQLException e){
            e.printStackTrace();
        } 
        return users;
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }

    public static void delete(int id){
        String sql = "DELETE from users where id = ?";

        try{
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
                
            
        } catch (SQLException e){
            e.printStackTrace();
        } 
    }
}
