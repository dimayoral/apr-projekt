package _20210312_DAO_Schuluebung.business;

import _20210312_DAO_Schuluebung.persistence.OrderDao;

public class App {    

    static String url = "jdbc:sqlite:./src/_20210205_DAO_Schuluebung/mydb.db";
    public static void main(String[] args) {
        OrderDao order = new OrderDao();
        order.connect();
        order.createNewTable();
        order.newOrder("Fritz Bauer", "20210312");
        }
}