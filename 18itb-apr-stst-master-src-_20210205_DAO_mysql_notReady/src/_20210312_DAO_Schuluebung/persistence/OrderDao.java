package _20210312_DAO_Schuluebung.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class OrderDao {
//Ich habe das Instance Scope gewählt
    static Connection conn;
    private static String url = "jdbc:sqlite:./src/_20210205_DAO_Schuluebung/mydb.db";

    public void connect() {
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }
    public void createNewTable() {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql = "CREATE TABLE IF NOT EXISTS orders (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	order_date date NOT NULL,\n"
                + "	customer varchar(255)\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void newOrder(String customer, String order_date){
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql = "INSERT INTO orders(customer,order_date) VALUES(?,?)";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, customer);
            pstmt.setString(2, order_date);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
