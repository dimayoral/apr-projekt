package _20210326_DAO_Schuluebung02.business;

import _20210326_DAO_Schuluebung02.persistence.MailDao;
import _20210326_DAO_Schuluebung02.persistence.UserDao;
import _20210326_DAO_Schuluebung02.persistence.models.Mail;
import _20210326_DAO_Schuluebung02.persistence.models.User;

public class App {    

    static String url = "jdbc:sqlite:./src/_20210325_Convert_To_DAO2/mydb.db";
    public static void main(String[] args) {
        MailDao mail = new MailDao();
        UserDao user = new UserDao();

        mail.newMail(new Mail("s.stanislaus@tsn.at", 1));
        mail.newMail(new Mail("s.stanislaus@gmail.com", 1));

        user.newUser(new User("Stanislaus"));

//Method Scope
    }
}

   