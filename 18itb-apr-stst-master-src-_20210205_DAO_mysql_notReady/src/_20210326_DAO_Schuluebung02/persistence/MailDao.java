package _20210326_DAO_Schuluebung02.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import _20210326_DAO_Schuluebung02.persistence.models.Mail;

public class MailDao {

    public MailDao(){
        
        DbController02.createNewTable();
    }
    public void newMail(Mail mail){
        DbController02.newMail(mail.getAddress(), mail.getPerson_id());
    }
}
class DbController02 {

    public static void newMail(String address, int person_id){
        Connection conn = null;
        String url = "jdbc:sqlite:./src/_20210325_Convert_To_DAO2/mydb.db";

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql = "INSERT INTO mails(address, person_id) VALUES(?, ?)";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, address);
            pstmt.setInt(2, person_id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createNewTable() {

        Connection conn = null;
        String url = "jdbc:sqlite:./src/_20210325_Convert_To_DAO2/mydb.db";

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql_persons = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer not null PRIMARY KEY,\n"
                + "	name varchar(255)\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql_persons);
            System.out.println("Table persons is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String sql_emails = "CREATE TABLE IF NOT EXISTS mails (\n"
                + "	id integer not null PRIMARY KEY,\n"
                + "	address varchar(255),\n"
                + "	person_id int\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql_emails);
            System.out.println("Table emails is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}