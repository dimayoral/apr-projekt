package _20210326_DAO_Schuluebung02.persistence.models;

public class Mail {
    private String address;
    private int person_id;
    
    public Mail(String string, int i) {

    }

    public String getAddress() {
        return address;
    }
    
    public int getPerson_id() {
        return person_id;
    }
    
    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
}
